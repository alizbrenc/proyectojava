package combina;

import java.util.ArrayList;
import java.util.Iterator;

public class listas {
	
	ArrayList<String> fdtps = new ArrayList<String>();
	ArrayList<String> fdtps0 = new ArrayList<String>();
	ArrayList<String> pronombresA = new ArrayList<String>();	
	/// presente simple 
	 public void  presenteS(String verbo)
	 {
		    System.out.println("PRESENTE SIMPLE");
			System.out.println(" ");			
			llenado(fdtps,fdtps0);			
			///AFIRMATIVO			
			ArrayList<String> presentesimpleA = new ArrayList<String>();			
			presentesimpleA = psimpleA(presentesimpleA,pronombresA,fdtps0,verbo);	     
			imprime(presentesimpleA);
			System.out.println("//NEGATIVO ");
		    //NEGATIVO 
			ArrayList<String> presentesimpleN = new ArrayList<String>();
			presentesimpleN = psimpleN(presentesimpleN,pronombresA,fdtps,verbo);	     
			imprime(presentesimpleN);
			System.out.println("//PS AFIRMATIVO INTERROGATIVO");
			ArrayList<String> presentesimpleIA = new ArrayList<String>();
			presentesimpleN = psimpleIA(presentesimpleIA,pronombresA,fdtps,verbo);	     
			imprime(presentesimpleN);
			
			System.out.println("///PS NEGATIVO INTERROGATIVO "); 
			ArrayList<String> presentesimpleIN = new ArrayList<String>();
			presentesimpleN = psimpleIN(presentesimpleIN,pronombresA,fdtps0,verbo);	     
			imprime(presentesimpleN);
			System.out.println(" "); 
	 }

	private void llenado(ArrayList<String> fdtps2,ArrayList<String>tdtps3) {
		
		fdtps2.add("th");
		fdtps2.add("ta");
		fdtps2.add("i");
		fdtps2.add("tan");
		fdtps2.add("th");
		fdtps2.add("ta");
		fdtps2.add("i");
		fdtps2.add("tan");	
		
		
		tdtps3.add("tha");
		tdtps3.add("ta");
		tdtps3.add("i");
		tdtps3.add("tana");
		tdtps3.add("tha");
		tdtps3.add("ta");
		tdtps3.add("i");
		tdtps3.add("tana");
	}

	private ArrayList<String> psimpleIN(ArrayList<String> presentesimpleIN, ArrayList<String> pronombresA2,ArrayList<String> fdt,
			String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);		
		int cont=0;
	    Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdt.iterator();
		while (iter.hasNext())
		{
		cont++;
         if (cont>4)
         {   String plu =aux2+"pxk"+iter2.next();
         presentesimpleIN.add((String) iter.next()+"xa"+ " "+ "janiti"+ " "+ plu); 
         }else
         {  
        	 String sin = aux1+"k"+iter2.next();
        	 presentesimpleIN.add((String) iter.next()+"xa"+" "+ "janiti"+ " "+ sin);
         }
        }
		return presentesimpleIN;
	}
	private ArrayList<String> psimpleIA(ArrayList<String> presentesimpleIA, ArrayList<String> pronombresA2,ArrayList<String> fdt,
			String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);		
		int cont=0;
	    Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdt.iterator();
		while (iter.hasNext())
		{
		cont++;
         if (cont>4)
         {   String plu = aux2+"px"+iter2.next()+"ti";
         presentesimpleIA.add((String) iter.next()+"xa"+" "+ plu); 
         }else
         {  
        	 String sin = aux1+iter2.next()+"ti";
          presentesimpleIA.add((String) iter.next()+"xa"+" "+ sin);
         }
        }
		return presentesimpleIA;
	}

	private ArrayList<String> psimpleN(ArrayList<String> presentesimpleN, ArrayList<String> pronombresA2,ArrayList<String> fdt,
			String verbo) 
	{
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);		
		int cont=0;
	    Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdt.iterator();
		while (iter.hasNext())
		{
		cont++;
         if (cont>4)
         {   String plu = aux2+"pxk"+iter2.next()+"ti";
        	 presentesimpleN.add((String) iter.next()+"xa"+ " "+ "janiwa"+ " "+ plu); 
         }else
         {  
        	 String sin = aux1+"k"+iter2.next()+"ti";
        	 presentesimpleN.add((String) iter.next()+"xa"+" "+ "janiwa"+ " "+ sin);
         }
        }
        
			return presentesimpleN;
	
	}

	private ArrayList<String> psimpleA(ArrayList<String> presentesimpleA, ArrayList<String> pronombresA2,ArrayList<String> fdt,
			String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);				
		int cont=0;		
		Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdt.iterator();
		
		while (iter.hasNext())
		{cont++;
         if (cont>4)
         {   String plu = aux2+"px"+iter2.next();
         presentesimpleA.add((String) iter.next()+"xa"+ " "+ plu); 
         }else
         {  
        	 String sin = aux1+iter2.next();
        	 presentesimpleA.add((String) iter.next()+"xa"+ " "+ sin);
         }
        }
        	
		return presentesimpleA;

	}

	public static  ArrayList<String> pronombres(ArrayList<String> listapronombres)
	{
		listapronombres.add("naya");
		listapronombres.add("juma");
		listapronombres.add("jupa");
		listapronombres.add("jiwasa");
		listapronombres.add("nänaka");
		listapronombres.add("jumanaka");
		listapronombres.add("jupanaka");
		listapronombres.add("jiwasanaka");
		
		return listapronombres;
	}
	
	public static void imprime(ArrayList<String> lista)
	{
		Iterator iter = lista.iterator();
		while (iter.hasNext())
		  System.out.println(iter.next());		
	}

	public void tiempocercano(String verbo) 
{
		
		System.out.println("Tiempo Remoto Cercano");
		System.out.println(" ");			
		llenado(fdtps,fdtps0);			
		
		ArrayList<String> tremotoA = new ArrayList<String>();			
		tremotoA = tremotoAfirmacion(tremotoA,pronombresA,fdtps0,verbo);	     
		imprime(tremotoA);
		System.out.println("//NEGATIVO ");	
		ArrayList<String> tremotoN = new ArrayList<String>();
		tremotoN = tremotoNegacion(tremotoN,pronombresA,fdtps,verbo);	     
		imprime(tremotoN);		
		System.out.println("// AFIRMATIVO INTERROGATIVO");
		ArrayList<String> tremotoAI = new ArrayList<String>();
		tremotoAI = tremotoAInterrogativo(tremotoAI,pronombresA,fdtps,verbo);	     
		imprime(tremotoAI);
		
		System.out.println("///NEGATIVO INTERROGATIVO "); 
		ArrayList<String> tremotoNI = new ArrayList<String>();
		tremotoNI = tremotoNInterrogativo(tremotoNI,pronombresA,fdtps0,verbo);	     
		imprime(tremotoNI);
		//System.out.println(" "); 
		
		
		
		
	}

	private ArrayList<String> tremotoNInterrogativo(ArrayList<String> tremotoNI, ArrayList<String> pronombresA2,
			ArrayList<String> fdtps02, String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);				
		int cont=0;		
		Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdtps02.iterator();
		String plu , au;
		while (iter.hasNext())
		{cont++; au=(String) iter2.next();
         if (cont>4)
         {  if(au=="i")
         {
        	 plu = aux2+"pxkana";
        	 tremotoNI.add((String) iter.next()+"xa"+ " "+ "janiti"+ " "+ plu); 
         }else {        	 
         plu = aux2+"pxkayä"+au;
         tremotoNI.add((String) iter.next()+"xa"+ " "+ "janiti"+ " "+  plu); 
         }
         }else
         {  if(au=="i")
         {
        	 String sin = aux1+"kana";
        	 tremotoNI.add((String) iter.next()+"xa"+ " "+ "janiti"+ " "+ sin);
         }else
         {
        	 String sin = aux1+"kayä"+au;
        	 tremotoNI.add((String) iter.next()+"xa"+ " "+ "janiti"+ " "+ sin);
         }
         }
        }
		
	
		return tremotoNI;
	}

	

	private ArrayList<String> tremotoAInterrogativo(ArrayList<String> tremotoAI, ArrayList<String> pronombresA2,
			ArrayList<String> fdtps2, String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);				
		int cont=0;		
		Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdtps2.iterator();
		String plu , au;
		while (iter.hasNext())
		{cont++; au=(String) iter2.next();
         if (cont>4)
         {  if(au=="i")
         {
        	 plu = aux2+"pxana"+"ti";
        	 tremotoAI.add((String) iter.next()+"xa"+ " "+ plu); 
         }else {        	 
         plu = aux2+"pxayä"+au+"ti";
         tremotoAI.add((String) iter.next()+"xa"+ " "+ plu); 
         }
         }else
         {  if(au=="i")
         {
        	 String sin = aux2+"na"+"ti";
        	 tremotoAI.add((String) iter.next()+"xa"+ " "+ sin);
         }else
         {
        	 String sin = aux2+"yä"+au+"ti";
        	 tremotoAI.add((String) iter.next()+"xa"+ " "+ sin);
         }
         }
        }
		return tremotoAI;
	}
	private ArrayList<String> tremotoNegacion(ArrayList<String> tremotoN, ArrayList<String> pronombresA2,
			ArrayList<String> fdtps2, String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);				
		int cont=0;		
		Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdtps2.iterator();
		String plu , au;
		while (iter.hasNext())
		{cont++; au=(String) iter2.next();
         if (cont>4)
         {  if(au=="i")
         {
        	 plu = aux2+"px"+"kana"+"ti";
        	 tremotoN.add((String) iter.next()+"xa"+ " "+ "janiwa"+ " "+ plu); 
         }else {        	 
         plu = aux2+"pxkayä"+au+"ti";
         tremotoN.add((String) iter.next()+"xa"+ " "+ "janiwa"+ " "+  plu); 
         }
         }else
         {  if(au=="i")
         {
        	 String sin = aux1+"kana"+"ti";
        	 tremotoN.add((String) iter.next()+"xa"+ " "+ "janiwa"+ " "+ sin);
         }else
         {
        	 String sin = aux1+"kayä"+au+"ti";
        	 tremotoN.add((String) iter.next()+"xa"+ " "+ "janiwa"+ " "+ sin);
         }
         }
        }
		
		
		return tremotoN;
	}

	private ArrayList<String> tremotoAfirmacion(ArrayList<String> tremotoA, ArrayList<String> pronombresA2,
			ArrayList<String> fdtps02, String verbo) {
		String aux1=verbo.substring(0,verbo.length()-3);
		String aux2=verbo.substring(0,verbo.length()-2);				
		int cont=0;		
		Iterator iter = pronombresA2.iterator();
		Iterator iter2 = fdtps02.iterator();
		String plu , au;
		while (iter.hasNext())
		{cont++; au=(String) iter2.next();
         if (cont>4)
         {  if(au=="i")
         {
        	 plu = aux2+"pxa"+"na";
             tremotoA.add((String) iter.next()+"xa"+ " "+ plu); 
         }else {        	 
         plu = aux2+"pxayä"+au;
         tremotoA.add((String) iter.next()+"xa"+ " "+ plu); 
         }
         }else
         {  if(au=="i")
         {
        	 String sin = aux2+"na";
        	 tremotoA.add((String) iter.next()+"xa"+ " "+ sin);
         }else
         {
        	 String sin = aux2+"yä"+au;
        	 tremotoA.add((String) iter.next()+"xa"+ " "+ sin);
         }
         }
        }
		
		
		return tremotoA;
	}

	

}
