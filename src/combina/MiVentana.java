package combina;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class MiVentana extends JFrame {
	public MiVentana() {
        super("Lenguas Nativas De Bolivia");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container cp = getContentPane();
       
        cp.setLayout(new FlowLayout());
      
        JRadioButton aymara = new JRadioButton ("Aymara");
        JRadioButton quechua = new JRadioButton ("quechua");
        JRadioButton guarani = new JRadioButton ("guarani");
        JRadioButton uru = new JRadioButton ("uru");
        JLabel etiqueta = new JLabel("Introdusca verbo : ");
        JTextField texto = new JTextField(20);
        JButton boton = new JButton("enviar"); 
        cp.add(quechua);
    
        cp.add(guarani);
        cp.add(uru);
        cp.add(aymara);
       
        cp.add(etiqueta);
        cp.add(texto);
        cp.add(boton);
	}
	

}
